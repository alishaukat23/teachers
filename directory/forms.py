from django import forms

class TeacherCSVUploadForm(forms.Form):
    csv_file = forms.FileField(label='Upload CSV file')