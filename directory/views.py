import csv
from django.shortcuts import get_object_or_404, render, redirect
from .models import Teacher
from .forms import TeacherCSVUploadForm

def teacher_list(request):
    teachers = Teacher.objects.all()
    context = {'teachers': teachers}
    return render(request, 'teacher_list.html', context)

def upload_teachers_csv(request):
    if request.method == 'POST':
        form = TeacherCSVUploadForm(request.POST, request.FILES)
        if form.is_valid():
            csv_file = request.FILES['csv_file']
            decoded_file = csv_file.read().decode('utf-8')
            csv_reader = csv.reader(decoded_file.splitlines(), delimiter=',')
            next(csv_reader)  # Skip header row if present
            
            for row in csv_reader:
                first_name = row[0]
                last_name = row[1]
                #profile_picture = row[2]  # Assuming this is the filename
                email = row[3]
                phone_number = row[4]
                room_number = row[5]
                subjects_taught = row[6]
                
                # Create and save Teacher instance
                teacher = Teacher(
                    first_name=first_name,
                    last_name=last_name,
                    email=email,
                    phone_number=phone_number,
                    room_number=room_number,
                    subjects_taught=subjects_taught
                )
                teacher.save()

            return redirect('teacher_list')
    else:
        form = TeacherCSVUploadForm()
    context = {'form': form}
    return render(request, 'upload_teachers_csv.html', context)

def teacher_detail(request, teacher_id):
    teacher = get_object_or_404(Teacher, pk=teacher_id)
    context = {'teacher': teacher}
    return render(request, 'directory/teacher_detail.html', context)