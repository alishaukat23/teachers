from django.urls import path
from . import views

urlpatterns = [
    path('', views.teacher_list, name='teacher_list'),
    path('upload-csv/', views.upload_teachers_csv, name='upload_teachers_csv'),
    path('teacher/<int:teacher_id>/', views.teacher_detail, name='teacher_detail'),

]
